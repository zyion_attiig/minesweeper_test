
FILES			= src/*.cpp
CC				= g++ -std=c++11 -g
COMPILER_FLAGS	= # -w -wl, -subsystem,windows
EXEC			= minesweeper

all: $(FILES)
	$(CC) $(FILES) $(COMPILER_FLAGS) -o $(EXEC)

clean:
	rm -f *.o
