
#include "solver.h"

Solver::Solver(Minefield* pMinefield) {
    this->state = State::START;
    this->turns = 0;
    this->opened = 0;
    this->minefield = pMinefield;
    this->minefield->GetSize(&this->width, &this->height);
    this->gamefield = (char**) malloc(this->height * sizeof(char*));
    for (int y = this->height; y-- > 0; ) {
        this->gamefield[y] = (char*) malloc((this->width + 1) * sizeof(char));
        for (int x = this->width; x-- > 0; ) {
            this->gamefield[y][x] = '_';
        }
        this->gamefield[y][this->width] = '\0';
    }
    this->minefield->GetMineCount(&this->mineCount);
    this->cellCount = this->width * this->height;
}

Solver::~Solver() {
    for (int i = this->height; i-- > 0; ) {
        free(this->gamefield[i]);
    }
    free(this->gamefield);
}



int Solver::checkBounds (unsigned int x, unsigned int y) {
    if (x >= 0 && x < this->width && y >= 0 && y < this->height) return 1;
    else return 0;
}

void Solver::canCheck (unsigned int x, unsigned int y) {
    if (this->checkBounds(x, y) == 1 && this->gamefield[y][x] == '_') CheckCell(x, y);
}

void Solver::CheckCells (unsigned int x, unsigned int y) {
    this->canCheck(x - 1, y - 1);
    this->canCheck(x - 1, y);
    this->canCheck(x - 1, y + 1);
    this->canCheck(x, y - 1);
    this->canCheck(x, y + 1);
    this->canCheck(x + 1, y - 1);
    this->canCheck(x + 1, y);
    this->canCheck(x + 1, y + 1);
}

void Solver::CheckCell (unsigned int x, unsigned int y) {
    Minefield::Cell cell = this->minefield->Open(x, y);
    this->gamefield[y][x] = Minefield::GetCellChar(cell);
    this->opened++;
    switch (cell) {
        case Minefield::Cell::MINE:
            this->state = State::DEAD;
            break;
        case Minefield::Cell::EMPTY:
            CheckCells(x, y);
        default:
            this->state = State::PLAY;
            break;
    }
}


void Solver::Open () {
    this->turns++;

    switch (this->state) {
        case START:
        case PLAY: {
            int x = rand() % this->width;
            int y = rand() % this->height;
            while (this->gamefield[y][x] != '_') {

                // improve cell selection
                x = rand() % this->width;
                y = rand() % this->height;

            }
            this->CheckCell(x, y);
        }
        default: break;
    }

    // check win condition
    if (this->state == State::PLAY && this->opened == this->cellCount - this->mineCount) {
        this->state = State::SOLVED;
    }
}

void Solver::Print () {
    std::cout << "MineField - " << this->width << " x " << this->height << '\n';
    for (int y = this->height; y-- > 0; ) {
        std::cout << '\t' << this->gamefield[y] << "\n";
    }
}

int Solver::GetTurns() {
    return this->turns;
}
