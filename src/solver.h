
#ifndef SOLVER
#define SOLVER

#include <cstdlib>
#include <ctime>
#include <iostream>

#include "minefield.h"

class Solver {
private:
    Minefield* minefield;
    unsigned int width, height;
    char** gamefield;
    unsigned int turns, mineCount, cellCount, opened;

    int checkBounds (unsigned int x, unsigned int y);
    void canCheck (unsigned int x, unsigned int y);
    void CheckCells (unsigned int x, unsigned int y);
    void CheckCell (unsigned int x, unsigned int y);

public:
    enum State {
        START,
        PLAY,
        DEAD,
        SOLVED
    };

    State state;

    Solver(Minefield* pMinefield);
    ~Solver();
    void Open ();
    void Print ();
    int GetTurns();
};

#endif
