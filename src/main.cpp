

#include "minefield.h"
#include "solver.h"


int main() {
    // Q1. generate minefield
    Minefield* minefield = GenerateMineField(10, 10, 10);
    minefield->print();

    // Q2. Solve minefield
    Solver* solver = new Solver(minefield);
    std::cout << "Minesweeper Game Board \n";
    solver->Print();
    while (solver->state == Solver::State::PLAY || solver->state == Solver::State::START) {
        solver->Open();
        std::cout << "\nTurn No. " << solver->GetTurns() << "\n";
        solver->Print();
        if (solver->state == Solver::State::SOLVED) {
            std::cout << "Congratulations! You Won in " << solver->GetTurns() << " turns... \n\n";
        }
        else if (solver->state == Solver::State::DEAD) {
            std::cout << "Boom! You Died in " << solver->GetTurns() << " turns... \n\n";
        }
    }

    // clean up
    delete solver;
    delete minefield;
    return 0;
}
