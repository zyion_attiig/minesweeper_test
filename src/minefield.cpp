
#include "minefield.h"

Minefield::Minefield(unsigned int width, unsigned int height, unsigned int mines) {
    this->width = width;
    this->height = height;
    this->mines = mines;
    // generate cell grid
    this->cells = (Minefield::Cell**) malloc(this->height * sizeof(Minefield::Cell*));
    for (int y = this->height; y-- > 0; ) {
        this->cells[y] = (Minefield::Cell*) malloc(this->width * sizeof(Minefield::Cell));
        for (int x = this->width; x-- > 0; ) {
            this->cells[y][x] = Minefield::Cell::EMPTY;
        }
    }
    // place random mines
    srand(time(NULL));
    for (int i = this->mines; i-- > 0; ) {
        int x = rand() % this->width;
        int y = rand() % this->height;
        while (this->cells[y][x] == Minefield::Cell::MINE) {
            x = rand() % this->width;
            y = rand() % this->height;
        }
        this->cells[y][x] = Minefield::Cell::MINE;
        // add mine indicators
        for (int iy = y - 1; iy <= y + 1; iy++) {
            // check in minefield vertical bounds
            if (iy >= 0 && iy < this->height) {
                for (int ix = x - 1; ix <= x + 1; ix++) {
                    // check in minefield horizontal bounds
                    if (ix >= 0 && ix < this->width) {
                        if (this->cells[iy][ix] != Minefield::Cell::MINE) {
                            int count = this->cells[iy][ix];
                            this->cells[iy][ix] = static_cast<Minefield::Cell>(++count);
                        }
                    }
                }
            }
        }


    }
}

Minefield::~Minefield() {
    for (int y = this->height; y-- > 0; ) {
        free(this->cells[y]);
    }
    free(this->cells);
}

char Minefield::GetCellChar (Minefield::Cell cell) {
    switch (cell) {
        case Minefield::Cell::M1: return '1';
        case Minefield::Cell::M2: return '2';
        case Minefield::Cell::M3: return '3';
        case Minefield::Cell::M4: return '4';
        case Minefield::Cell::M5: return '5';
        case Minefield::Cell::M6: return '6';
        case Minefield::Cell::M7: return '7';
        case Minefield::Cell::M8: return '8';
        case Minefield::Cell::M9: return '9';
        case Minefield::Cell::MINE: return 'M';
        case Minefield::Cell::EMPTY:
        default: return ',';
    }
}

void Minefield::print() {
    std::cout << "\nMineField - " << this->width << " x " << this->height << " x " << this->mines << '\n';
    for (int y = this->height; y-- > 0; ) {
        char line[this->width + 1];
        for (int x = 0; x < this->width; x++) {
            line[x] = GetCellChar(this->Open(x, y));
        }
        line[this->width] = '\0';
        std::cout << '\t' << line << '\n';
    }
    std::cout << '\n';
}

void Minefield::GetSize(unsigned int* width, unsigned int* height){
    *width = this->width;
    *height = this->height;
}

void Minefield::GetMineCount(unsigned int* count){
    *count = this->mines;
}

Minefield::Cell Minefield::Open(unsigned int x, unsigned int y){
    return this->cells[y][x];
}

Minefield* GenerateMineField(unsigned int width, unsigned int height, unsigned int count) {
    return new Minefield (width, height, count);
}
