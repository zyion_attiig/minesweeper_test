
#ifndef MINEFIELD
#define MINEFIELD

#include <cstdlib>
#include <ctime>
#include <iostream>

class Minefield {
public:
    enum Cell {
        EMPTY,
        M1,
        M2,
        M3,
        M4,
        M5,
        M6,
        M7,
        M8,
        M9,
        MINE
    };
    Minefield(unsigned int, unsigned int, unsigned int);
    ~Minefield();
    void print();
    void GetSize(unsigned int*, unsigned int*);
    void GetMineCount(unsigned int*);
    Minefield::Cell Open(unsigned int, unsigned int);
    static char GetCellChar (Minefield::Cell);
private:
    unsigned int width, height, mines;
    Minefield::Cell **cells;
};

Minefield* GenerateMineField(unsigned int, unsigned int, unsigned int);

#endif
